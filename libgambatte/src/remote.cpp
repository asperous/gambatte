#include "remote.h"
#include "memory.h"


namespace gambatte {

Remote::Remote()
: lastCC(0)
, remoteInput()
, context (1)
, publisher (context, ZMQ_PUB)
{
    publisher.connect("tcp://127.0.0.1:5555");
}

    unsigned char lastX = 0;
    unsigned char lastY = 0;

void Remote::sendUpdate(Memory *mem_, unsigned long cc) {
    unsigned width = mem_->read(0xD369, cc);
    unsigned height = mem_->read(0xD368, cc);
    if (width && height) {
        unsigned char x = (unsigned char) (100 * mem_->read(0xD362, cc) / (width * 2));
        unsigned char y = (unsigned char) (100 * mem_->read(0xD361, cc) / (height * 2));
        unsigned char map_number = (unsigned char) mem_->read(0xD35E, cc);

        if (x != lastX || y != lastY) {
            lastX = x;
            lastY = y;
            zmq::message_t message(4);
            char * msg = (char *) message.data();
            msg[0] = x;
            msg[1] = y;
            msg[2] = map_number;
            msg[3] = 2;
            publisher.send(message);
        }
    }
}

void Remote::updateInputIfChanged(unsigned long cc) {
    unsigned lastInput = 0;
    if (cc > lastCC) {
        f.read((char *) &lastCC, sizeof(unsigned long));
        f.read((char *) &lastInput, sizeof(unsigned));
        remoteInput.is = lastInput;
    }
}
}
