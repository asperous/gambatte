//
//   Copyright (C) 2007 by andychase
//
//   This program is free software; you can redistribute it and/or modify
//   it under the terms of the GNU General Public License version 2 as
//   published by the Free Software Foundation.
//
//   This program is distributed in the hope that it will be useful,
//   but WITHOUT ANY WARRANTY; without even the implied warranty of
//   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//   GNU General Public License version 2 for more details.
//
//   You should have received a copy of the GNU General Public License
//   version 2 along with this program; if not, write to the
//   Free Software Foundation, Inc.,
//   51 Franklin St, Fifth Floor, Boston, MA  02110-1301, USA.
//

#ifndef REMOTE_H
#define REMOTE_H

#include <iostream>
#include <fstream>
#include <inputgetter.h>
#include <zmq.hpp>
#include "memory.h"


namespace gambatte {

struct RemoteInput : InputGetter {
    unsigned is;
    RemoteInput() : is(0) {}
    virtual unsigned operator()() { return is; }
};

class Remote {
public:
    explicit Remote();
    void updateInputIfChanged(unsigned long cc);

    std::ifstream f;
    unsigned long lastCC;
    RemoteInput remoteInput;
    zmq::context_t context;
    zmq::socket_t publisher;

    void sendUpdate(gambatte::Memory *mem_, unsigned long cc);
};


}

#endif