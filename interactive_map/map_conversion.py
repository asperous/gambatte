from lxml import etree
import json
import sys

filename = sys.argv[1]
out_filename = sys.argv[2]
output = {}

with open(filename, 'r') as infile: 
    tree = etree.parse( infile )

root = tree.getroot()

g = root.find("{http://www.w3.org/2000/svg}g")
pageX, pageY = [float(i) for i in g.attrib['transform'][10:-1].split(',')]

rects = g.findall("{http://www.w3.org/2000/svg}rect")

def get_output():
	for rect in rects:
		d = rect.attrib
		label = d['{http://www.inkscape.org/namespaces/inkscape}label']
		if "." not in label:
			label = "3." + label
		yield label, {
			'x': float(d['x'])+pageX,
			'y': float(d['y'])+pageY,
			'height': float(d['height']),
			'width': float(d['width']),
		}


with open(out_filename, 'w') as outfile:
	json.dump(
		dict(get_output()),
		outfile,
		sort_keys=True,
		indent=4,
		separators=(',', ': ')
	)

# Prepend "translation = "
with open(out_filename, 'r') as outfile:
	text_data = outfile.read()

with open(out_filename, 'w') as outfile:
	outfile.write("translation = " + text_data)
