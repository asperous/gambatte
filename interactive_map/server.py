import os
import time
import math
import json
import struct

import gevent
import zmq.green as zmq
from geventwebsocket.handler import WebSocketHandler

format = "B" * 6

def main():
    context = zmq.Context()

    ws_server = gevent.pywsgi.WSGIServer(
        ('', 9999), WebSocketApp(context),
        handler_class=WebSocketHandler)

    ws_server.start()

    zmq_server(context)
    
def zmq_server(context):
    sock_outgoing = context.socket(zmq.PUB)
    sock_outgoing.bind('inproc://queue')

    sock_incoming = context.socket(zmq.SUB)
    sock_incoming.setsockopt(zmq.SUBSCRIBE, "")
    sock_incoming.bind("tcp://*:5555")

    while True:
        msg = sock_incoming.recv()
        sock_outgoing.send(msg)

class WebSocketApp(object):
    def __init__(self, context):
        self.context = context

    def __call__(self, environ, start_response):
        ws = environ['wsgi.websocket']
        sock = self.context.socket(zmq.SUB)
        sock.setsockopt(zmq.SUBSCRIBE, "")
        sock.connect('inproc://queue')
        while True:
            msg = struct.unpack(format, sock.recv())
            ws.send(json.dumps(msg))

if __name__ == '__main__':
    main()
