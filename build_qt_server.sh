#!/bin/sh

echo "cd libgambatte && scons"
(cd libgambatte && scons) || exit

echo "cd gambatte_qt && rm -fr bin && qmake && make"
(cd gambatte_qt && rm -fr bin && qmake "SERVER=true" && make)

gambatte_qt/bin/Gambatte\ Qt.app/Contents/MacOS/Gambatte\ Qt $@
