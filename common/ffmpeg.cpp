#include <cstdio>
#include <iostream>
#include "ffmpeg.h"

MediaRecorder::MediaRecorder()
: pts (0)
, context (1)
, publisher (context, ZMQ_PUB)
{
    publisher.connect("tcp://127.0.0.1:5556");
    avcodec_register_all();
}

void MediaRecorder::Record(const char *filename) {
    AVCodec *codec;
    int ret;

    printf("Encode video file %s\n", filename);
    /* find the mpeg1 video encoder */
    AVCodecID codec_id = AV_CODEC_ID_H264;
    codec = avcodec_find_encoder(codec_id);
    if (!codec) {
        fprintf(stderr, "Codec not found\n");
        exit(1);
    }
    c = avcodec_alloc_context3(codec);
    if (!c) {
        fprintf(stderr, "Could not allocate video codec context\n");
        exit(1);
    }
    /* put sample parameters */
    c->bit_rate = 400000;
    /* resolution must be a multiple of two */
    c->width = 160;
    c->height = 144;
    /* frames per second */
    c->time_base = (AVRational) {1, 60};
    /* emit one intra frame every ten frames
     * check frame pict_type before passing frame
     * to encoder, if frame->pict_type is AV_PICTURE_TYPE_I
     * then gop_size is ignored and the output of encoder
     * will always be I frame irrespective to gop_size
     */
    c->gop_size = 10;
//    c->max_b_frames = 1;
    c->pix_fmt = AV_PIX_FMT_YUV420P;
    av_opt_set(c->priv_data, "preset", "ultrafast", 0);
    av_opt_set(c->priv_data, "crf", "23", 0);
    /* open it */
    if (avcodec_open2(c, codec, NULL) < 0) {
        fprintf(stderr, "Could not open codec\n");
        exit(1);
    }
    frame = av_frame_alloc();
    if (!frame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }
    frame->format = c->pix_fmt;
    frame->width = c->width;
    frame->height = c->height;
    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    ret = av_image_alloc(frame->data, frame->linesize, c->width, c->height,
                         c->pix_fmt, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate raw picture buffer\n");
        exit(1);
    }
    // Setup src frame
    srcFrame = av_frame_alloc();
    if (!srcFrame) {
        fprintf(stderr, "Could not allocate video frame\n");
        exit(1);
    }
    srcFrame->format = c->pix_fmt;
    srcFrame->width = c->width;
    srcFrame->height = c->height;
    /* the image can be allocated by any means and av_image_alloc() is
     * just the most convenient way if av_malloc() is to be used */
    ret = av_image_alloc(srcFrame->data, srcFrame->linesize, c->width, c->height,
                         AV_PIX_FMT_BGRA, 32);
    if (ret < 0) {
        fprintf(stderr, "Could not allocate raw picture buffer\n");
        exit(1);
    }

    ctx = sws_getContext(frame->width, frame->height,
                                      AV_PIX_FMT_BGRA, frame->width, frame->height,
                                      AV_PIX_FMT_YUV420P, 0, 0, 0, 0);
}

void MediaRecorder::Stop() {
    int got_output = 0, ret = 0;
    AVPacket pkt;
    av_init_packet(&pkt);
    pkt.data = NULL;    // packet data will be allocated by the encoder
    pkt.size = 0;

    /* get the delayed frames */
    for (got_output = 1; got_output;) {
        fflush(stdout);
        ret = avcodec_encode_video2(c, &pkt, NULL, &got_output);
        if (ret < 0) {
            fprintf(stderr, "Error encoding frame\n");
            exit(1);
        }
        if (got_output) {
            zmq::message_t message((size_t) pkt.size);
            memcpy(message.data(), pkt.data, (size_t) pkt.size);
            publisher.send(message);
            av_packet_unref(&pkt);
        }
    }

    uint8_t endcode[] = {0, 0, 1, 0xb7};
    /* add sequence end code to have a real mpeg file */
//    fwrite(endcode, 1, sizeof(endcode), f);
}

MediaRecorder::~MediaRecorder() {
    Stop();
}

// Still needs updating for avcodec_encode_video2
void MediaRecorder::AddFrame(uint32_t *vid) {
    AVPacket pkt;
    int x, y, ret, got_output;

    av_init_packet(&pkt);
    pkt.data = NULL;    // packet data will be allocated by the encoder
    pkt.size = 0;

    av_image_fill_arrays(
            srcFrame->data,
            srcFrame->linesize,
            (uint8_t *) vid,
            AV_PIX_FMT_BGRA,
            srcFrame->width,
            srcFrame->height,
            32
    );

    sws_scale(ctx, (const uint8_t *const *) srcFrame->data, srcFrame->linesize, 0, frame->height, frame->data, frame->linesize);

//    /* encode the image */
    frame->pts = (int64_t) ((1.0 / 60) * 90 * pts++);

    ret = avcodec_encode_video2(c, &pkt, frame, &got_output);
    if (ret < 0) {
        fprintf(stderr, "Error encoding frame\n");
        exit(1);
    }
    if (got_output) {
        zmq::message_t message((size_t) pkt.size);
        memcpy(message.data(), pkt.data, (size_t) pkt.size);
        publisher.send(message);
        av_packet_unref(&pkt);
    }
}
