#ifndef WX_FFMPEG_H
#define WX_FFMPEG_H

// simplified interface for recording audio and/or video from emulator

// unlike the rest of the wx code, this has no wx dependency at all, and
// could be used by other front ends as well.

// this only supports selecting output format via file name extensions;
// maybe some future version will support specifying a format.  wx-2.9
// has an extra widget for the file selector, but 2.8 doesn't.

// the only missing piece that I couldn't figure out how to do generically
// is the code to find the available formats & associated extensions for
// the file dialog.

#include <zmq.hpp>

#ifndef __VBA_TYPES_H__
#define __VBA_TYPES_H__

extern "C" { ;
#include <math.h>
#include <libavutil/opt.h>
#include <libavcodec/avcodec.h>
#include <libavutil/channel_layout.h>
#include <libavutil/common.h>
#include <libavutil/imgutils.h>
#include <libavutil/mathematics.h>
#include <libavutil/samplefmt.h>
#include <libswscale/swscale.h>
}

#define INBUF_SIZE 4096
#define AUDIO_INBUF_SIZE 20480

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

#endif // __VBA_TYPES_H__


class MediaRecorder
{
public:
    MediaRecorder();
    virtual ~MediaRecorder();

    // start audio only
    void Record(const char *fname);
    // stop both
    void Stop();
    // add a frame of video; width+height+depth already given
    // assumes a 1-pixel border on top & right
    // always assumes being passed 1/60th of a second of video
    void AddFrame(uint32_t *vid);
    // add a frame of audio; uses current sample rate to know length
    // always assumes being passed 1/60th of a second of audio.

private:
    AVCodecContext *c;
    AVFrame *frame;
    AVFrame *srcFrame;
    struct SwsContext *ctx;
    uint64_t pts;
    zmq::context_t context;
    zmq::socket_t publisher;
};

#endif /* WX_FFMPEG_H */
